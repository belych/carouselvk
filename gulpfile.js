const gulp = require('gulp');
const gulpSass = require('gulp-sass');
const sass = require('node-sass');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');

gulpSass.compile = sass;

const sass_path = './sass/**/*.scss';
const css_path = './css';
const js_path = './js/**/*.js';

gulp.task('sass', () => {
    return gulp.src(sass_path)
        .pipe(gulpSass().on('error', gulpSass.logError))
        .pipe(concat('index.css'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(css_path));
});

gulp.task('scripts', ()=>{
    return gulp.src(js_path)
    .pipe(concat('index.js'))
    .pipe(gulp.dest('./build/js'));
});

gulp.task('sass:watch', () => {
    gulp.watch(sass_path, gulp.series(['sass']));
});

gulp.task('scripts:watch', ()=> {
    gulp.watch(js_path, gulp.series(['scripts']))
});

gulp.task('dev', gulp.series(['sass', 'scripts', gulp.parallel(['scripts:watch' ,'sass:watch'])]));
