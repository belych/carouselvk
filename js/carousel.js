/**
 * Методы апи
 */
const API_METHODS = {
    getPhotos: 'photos.get',
}

/**
 * Максимально допустимый размер фотографии в карусели
 */
const MAX_PHOTO_HEIGHT = 604;

/**
 * Адрес сайта приложения
 */
const APP_URL = 'http://127.0.0.1:5500';

/**
 * Id приложения
 */
const APP_ID = 6760501;

/**
 * Получение токена
 */
function getAccess_token() {
    let url = window.location.href;
    if (url.indexOf('access_token') == -1) {
        $('.modal-link').trigger('click');
    }
    let access_token = url
        .split("#")[1]
        .split("=")[1]
        .split("&")[0];

    return access_token;
}

/**
 * Перемещение кнопок меню
 */
function movingButtons() {
    $(".button-auth").hide('hide');
    $(".button-photo").hide('hide');
    $(".button-out").css({
        "position": "absolutle",
        "top": "-20px",
        "left": "0",
        "margin": "5px",
    });
    $(".menu").css("margin", "0");
}

/**
 * Создание запроса
 * @param {string} method - метод апи
 * @param {{}} params - параметры запроса
 */
function getUrl(method, params) {
    const token = getAccess_token();
    if (!token) {
        $('.modal-link').trigger('click');
    }
    if (!method) throw new Error('Вы не указали метод');
    movingButtons();
    params = params || {};
    params['access_token'] = token;
    return `https://api.vk.com/method/${method}?${$.param(params)}&v=5.92`;
}

/**
 * Отправка запроса
 * @param {string} method - метод апи
 * @param {{}} params - параметры запроса
 * @param {function} func - callback при удачном запросе
 */
function sendRequest(method, params, func) {
    $.ajax({
        url: getUrl(method, params),
        method: 'GET',
        dataType: 'JSONP',
        success: func
    });
}

/**
 * Загружает карусель фотографий
 */
function loadPhotos() {
    sendRequest(API_METHODS.getPhotos, {
        album_id: 'wall'
    }, function (data) {
        drawPhotos(data.response.items);
    });
}

/**
 * Добавляет фотографии в карусель
 * @param {[any]} photos 
 */
function drawPhotos(photos) {
    photos.forEach(({
        sizes
    }) => {
        let max_height = 0;
        let url_photo;

        sizes.forEach((currentPhoto) => {
            if (max_height < currentPhoto.height &&
                currentPhoto.height <= MAX_PHOTO_HEIGHT) {
                url_photo = currentPhoto.url;
                max_height = currentPhoto.height;
            }
        });

        const photoContainer =
            `<div>
                <div class="container-img">
                    <img src=${url_photo}> 
                </div> 
            </div>`;
        $('.carousel').slick('slickAdd', photoContainer);
    })

    $('#slick-slide-control00').css('display', 'block') // показать dots
}

/**
 * Удаление токена, переход на главную страницу
 */
function logout() {
    $(location).attr('href', APP_URL);
}

/**
 * Авторизация
 */
function auth() {
    let url = `https://oauth.vk.com/authorize?client_id=${APP_ID}&display=page&redirect_uri=${APP_URL}&scope=photos&response_type=token&v=5.92`;
    $(location).attr('href', url);
}